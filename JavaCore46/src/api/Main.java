package api;

import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
		int result = Stream.of(text.split(" ")).filter(word -> word.length() > 4).mapToInt(String::length).reduce(0,
				Integer::sum);
		System.out.println("The total number of letters in words longer than 4 letters is " + result);
	}
}
