package numbercomparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

		List<Integer> list = new ArrayList<>();
		Random random = new Random();

		for (int i = 0; i < 20; i++) {
			int randomInt = random.nextInt(21) - 10;
			list.add(randomInt);
		}
		System.out.println("Initial Array: " + list);
		System.out.println();

		Comparator<Integer> comp1 = (a, b) -> Math.abs(a) - Math.abs(b);
		Comparator<Integer> comp2 = (a, b) -> a - b;

		list.sort(comp1.thenComparing(comp2.reversed()));
		System.out.println("Sorted Array: " + list);
		System.out.println();

		System.out.println("Closest number to zero: " + list.get(0));
		System.out.println();
	}

}
