package api;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		Random rand = new Random();
		List<Integer> randomNumbers = rand.ints(20, 0, 100).boxed().collect(Collectors.toList());
		System.out.println("Original list: " + randomNumbers);
		List<Integer> oddNumbers = randomNumbers.stream().filter(n -> n % 2 != 0).sorted().collect(Collectors.toList());
		System.out.println("Sorted list of odd numbers: " + oddNumbers);
	}
}
