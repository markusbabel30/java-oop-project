package comparator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

class SumDigitsComparator implements Comparator<Integer> {
	public int compare(Integer a, Integer b) {
		int aSum = (a % 10) + (a / (int) Math.pow(10, (int) (Math.log10(a) + 1) - 1));
		int bSum = (b % 10) + (b / (int) Math.pow(10, (int) (Math.log10(b) + 1) - 1));
		return Integer.compare(aSum, bSum);
	}
}

public class Main {
	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<>();
		Random rand = new Random();
		for (int i = 0; i < 10; i++) {
			numbers.add(rand.nextInt(2001));
		}
		System.out.println("Before sorting: " + numbers);
		Collections.sort(numbers, new SumDigitsComparator());
		System.out.println("After sorting: " + numbers);
	}
}
