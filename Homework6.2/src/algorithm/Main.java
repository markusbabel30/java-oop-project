package algorithm;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.Random;

public class Main {
  public static void main(String[] args) {
    // Create an array of 10,000 random integers
    int[] array = new int[10000];
    Random random = new Random();
    for (int i = 0; i < array.length; i++) {
      array[i] = random.nextInt();
    }

    // Calculate the sum using a simple algorithm
    long startTime = System.currentTimeMillis();
    int sum = 0;
    for (int i = 0; i < array.length; i++) {
      sum += array[i];
    }
    long endTime = System.currentTimeMillis();
    long simpleAlgorithmTime = endTime - startTime;
    System.out.println("Sum using simple algorithm: " + sum + " (took " + simpleAlgorithmTime + " milliseconds)");

    // Calculate the sum using a multi-threaded algorithm
    startTime = System.currentTimeMillis();
    int numThreads = Runtime.getRuntime().availableProcessors();
    ForkJoinPool pool = new ForkJoinPool(numThreads);
    SumTask sumTask = new SumTask(array, 0, array.length - 1);
    int multiThreadedSum = pool.invoke(sumTask);
    endTime = System.currentTimeMillis();
    long multiThreadedTime = endTime - startTime;
    System.out.println("Sum using multi-threaded algorithm: " + multiThreadedSum + " (took " + multiThreadedTime + " milliseconds)");
  }
}

class SumTask extends RecursiveTask<Integer> {
  private static final int THRESHOLD = 10;
  private int[] array;
  private int start;
  private int end;

  public SumTask(int[] array, int start, int end) {
    this.array = array;
    this.start = start;
    this.end = end;
  }

  @Override
  protected Integer compute() {
    if (end - start < THRESHOLD) {
      int sum = 0;
      for (int i = start; i <= end; i++) {
        sum += array[i];
      }
      return sum;
    } else {
      int mid = start + (end - start) / 2;
      SumTask left = new SumTask(array, start, mid);
      SumTask right = new SumTask(array, mid + 1, end);
      left.fork();
      right.fork();
      return left.join() + right.join();
    }
  }
}
