package calendar;

import java.util.Calendar;
import java.util.function.Function;

public class Main {
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		Function<Calendar, Integer> getYear = (c) -> c.get(Calendar.YEAR);
		System.out.println(getYear.apply(calendar));
	}
}