package availability;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SiteAvailabilityChecker {

    private static List<String> readUrlsFromFile(String fileName) {
        List<String> urls = new ArrayList<>();
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader reader = new BufferedReader(fileReader)) {
            String url;
            while ((url = reader.readLine()) != null) {
                urls.add(url);
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found: " + fileName);
        } catch (IOException e) {
            System.err.println("Error while reading file: " + e.getMessage());
        }
        return urls;
    }

    private static boolean checkAvailability(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");
            return connection.getResponseCode() == 200;
        } catch (MalformedURLException e) {
            System.err.println("Invalid URL: " + url);
            return false;
        } catch (ConnectException e) {
            System.err.println("Error connecting to: " + url + ": " + e.getMessage());
            return false;
        } catch (IOException e) {
            System.err.println("Error while checking availability: " + e.getMessage());
            return false;
        }
    }

    private static void writeResultToFile(String fileName, String url, boolean isAvailable) {
        try (FileWriter writer = new FileWriter(fileName, true)) {
            String availability = isAvailable ? "is available" : "is not available";
            writer.write(url + " " + availability + "\n");
        } catch (IOException e) {
            System.err.println("Error while writing to file: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        String fileName = "sites.txt";
        List<String> urls = readUrlsFromFile(fileName);
        for (String url : urls) {
            boolean isAvailable = checkAvailability(url);
            if (isAvailable) {
                System.out.println(url + " is available");
            } else {
                System.out.println(url + " is not available");
            }
            writeResultToFile("results.txt", url, isAvailable);
        }
    }
}
