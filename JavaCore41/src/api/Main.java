package api;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		String text = "this is an example of sorting text by the number of vowels";

		Stream<String> words = Arrays.stream(text.split(" "));
		words.filter(w -> w.matches(".*[aeiouAEIOU].*"))
				.sorted(Comparator.comparingInt(w -> w.replaceAll("[^aeiouAEIOU]", "").length()))
				.forEach(System.out::println);
	}
}
