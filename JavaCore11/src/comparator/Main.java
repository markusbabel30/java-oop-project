package comparator;

import java.util.Arrays;
import java.util.Comparator;

class Cat {
	private String name;
	private int age;

	public Cat(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + ", age=" + age + "]";
	}
}

public class Main {
	public static void main(String[] args) {
		Cat cat1 = new Cat("Umka", 12);
		Cat cat2 = new Cat("Luska", 5);
		Cat cat3 = new Cat("Barsic", 5);
		Cat cat4 = new Cat("Timka", 5);
		Cat cat5 = new Cat("Kuzia", 2);
		Cat[] cats = new Cat[] { cat1, cat2, cat3, cat4, cat5 };

		System.out.println("Original Array: " + Arrays.toString(cats));
		Arrays.sort(cats, new Comparator<Cat>() {
			@Override
			public int compare(Cat c1, Cat c2) {
				return Integer.compare(c1.getName().length(), c2.getName().length());
			}
		});
		System.out.println("Sorted Array: " + Arrays.toString(cats));
	}
}
