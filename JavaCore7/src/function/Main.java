package function;

import java.util.Scanner;
import java.util.function.Function;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a text: ");
		String text = scanner.nextLine();
		text = text.replaceAll("[^a-zA-Z]", ""); // Remove special characters and spaces
		Function<String, Integer> consonantCounter = new ConsonantCounter();
		int result = consonantCounter.apply(text);
		System.out.println("Number of consonants in the text: " + result);
	}
}

class ConsonantCounter implements Function<String, Integer> {
	@Override
	public Integer apply(String text) {
		int count = 0;
		for (char c : text.toCharArray()) {
			if (!(c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y' || c == 'Y' || c == 'A'
					|| c == 'E' || c == 'I' || c == 'O' || c == 'U')) {
				count++;
			}
		}
		return count;
	}
}
