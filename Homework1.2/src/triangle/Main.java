package triangle;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Triangle geron = new Triangle();

		System.out.println("Enter side A of the triangle:");
		geron.setSideA(sc.nextDouble());
		System.out.println("Enter side B of the triangle:");
		geron.setSideB(sc.nextDouble());
		System.out.println("Enter side C of the triangle:");
		geron.setSideC(sc.nextDouble());
		System.out.println(geron.toString());
		geron.calculation();

		Triangle newGeron = new Triangle(5.43, 5.67, 5.46);
		System.out.println(newGeron.toString());
		newGeron.calculation();
		sc.close();
	}

}