package comparable;

import java.util.Arrays;
import java.util.Random;

public class Main {
	public static void main(String[] args) {
		Rectangle[] rectangles = new Rectangle[5];
		Random random = new Random();

		for (int i = 0; i < rectangles.length; i++) {
			int length = random.nextInt(10) + 1;
			int height = random.nextInt(10) + 1;
			rectangles[i] = new Rectangle(length, height);
		}

		Arrays.sort(rectangles);

		for (Rectangle rectangle : rectangles) {
			System.out.println("Length: " + rectangle.getLength() + " Height: " + rectangle.getHeight() + " Area: "
					+ rectangle.getArea());
		}
	}
}

class Rectangle implements Comparable<Rectangle> {
	private int length;
	private int height;

	public Rectangle(int length, int height) {
		this.length = length;
		this.height = height;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getArea() {
		return length * height;
	}

	@Override
	public int compareTo(Rectangle other) {
		return Integer.compare(this.getArea(), other.getArea());
	}
}
