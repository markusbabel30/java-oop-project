package studentgroup;

public class Main {
	public static void main(String[] args) {

		String groupName = "MyGroup";
		Group group = new Group(groupName);

		Student student01 = StudentBuilder.createStudent();
		try {
			StudentBuilder.addToGroup(student01, group);
		} catch (GroupOverflowException e) {
			System.out.println("e.getMessage()");
		}

		CSVStringConverter csv = new CSVStringConverter();
		String studentCSV = csv.toStringRepresentation(student01);
		Student student02 = csv.fromStringRepresentation(studentCSV);

		try {
			StudentBuilder.addToGroup(student02, group);
		} catch (GroupOverflowException e) {
			System.out.println("e.getMessage()");
		}

		printGroupInfo(group);
	}

	private static void printGroupInfo(Group group) {
		System.out.println("-------------------------------------------");
		System.out.println(group);
		System.out.println("-------------------------------------------");
	}
}
