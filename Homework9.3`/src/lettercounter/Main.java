package lettercounter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    
    private static String readTextFromFile(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        return reader.lines().collect(Collectors.joining("\n"));
    }
    
    private static Map<Character, Long> calculateLetterFrequency(String text) {
        return text.chars()
            .mapToObj(c -> Character.toLowerCase((char) c))
            .filter(Character::isLetter)
            .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    }
    
    private static Map<Character, Long> sortFrequencyMap(Map<Character, Long> frequency) {
        return frequency.entrySet()
            .stream()
            .sorted(Map.Entry.<Character, Long>comparingByValue().reversed())
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                      (e1, e2) -> e1, LinkedHashMap::new));
    }
    
    private static void printResult(Map<Character, Long> frequency) {
        for (Map.Entry<Character, Long> entry : frequency.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    public static void main(String[] args) throws IOException {
        String fileName = "text.txt";
        String text = readTextFromFile(fileName);
        Map<Character, Long> frequency = calculateLetterFrequency(text);
        Map<Character, Long> sortedFrequency = sortFrequencyMap(frequency);
        printResult(sortedFrequency);
    }
}
