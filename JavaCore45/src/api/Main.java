package api;

import java.util.List;
import java.util.Scanner;
import java.util.Optional;

public class Main {

	public enum DifficultyLevel {
		HARD, MEDIUM, EASY
	}

	public static class ProgrammingLanguage {
		private String name;
		private DifficultyLevel difficulty;

		public ProgrammingLanguage(String name, DifficultyLevel difficulty) {
			this.name = name;
			this.difficulty = difficulty;
		}

		public String getName() {
			return name;
		}

		public DifficultyLevel getDifficulty() {
			return difficulty;
		}
	}

	public static void main(String[] args) {
		List<ProgrammingLanguage> languages = List.of(new ProgrammingLanguage("Haskell", DifficultyLevel.HARD),
				new ProgrammingLanguage("Python", DifficultyLevel.EASY),
				new ProgrammingLanguage("Java", DifficultyLevel.MEDIUM),
				new ProgrammingLanguage("C++", DifficultyLevel.HARD),
				new ProgrammingLanguage("JS", DifficultyLevel.EASY));

		Scanner scanner = new Scanner(System.in);
		System.out.println("Select a difficulty level (HARD, MEDIUM, EASY): ");
		String desiredDifficulty = scanner.nextLine();

		Optional<ProgrammingLanguage> result = languages.stream()
				.filter(lang -> lang.getDifficulty().name().equalsIgnoreCase(desiredDifficulty)).findAny();

		if (result.isPresent()) {
			System.out.println("Selected language: " + result.get().getName());
		} else {
			System.out.println("No language with that difficulty level was found.");
		}
	}
}
