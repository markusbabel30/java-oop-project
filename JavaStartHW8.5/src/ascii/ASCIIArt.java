package ascii;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ASCIIArt {

	public static void main(String[] args) {
		createASCIIArt("fractal.txt");
	}

	public static void createASCIIArt(String fileName) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			for (int i = 0; i < 40; i++) {
				for (int j = 0; j < 40; j++) {
					// Generate the fractal pattern using the Mandelbrot set
					double real = (j - 20) * 0.1;
					double imag = (i - 20) * 0.1;
					double x = 0, y = 0;
					int iterations = 0;
					while (x * x + y * y < 4 && iterations < 10) {
						double xNew = x * x - y * y + real;
						y = 2 * x * y + imag;
						x = xNew;
						iterations++;
					}
					if (iterations < 10) {
						bw.write("#");
						System.out.print("#");
					} else {
						bw.write(" ");
						System.out.print(" ");
					}
				}
				bw.newLine();
				System.out.println();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
