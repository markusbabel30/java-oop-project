package animals;

public class Main {

	public static void main(String[] args) {

		Cat cat1 = new Cat("catfish", "white", 2, "Bobb");
		Cat cat2 = new Cat("jellyfish", "gray", 4, "Bill");
		Cat cat3 = new Cat("dogfish", "red", 3, "Bard");

		Dog dog1 = new Dog("hotdog", "brown", 6, "Morbid");
		Dog dog2 = new Dog("meatball", "black", 8, "Massacre");
		Dog dog3 = new Dog("pizza", "gold", 7, "Maybell");

		System.out.println(cat1);
		System.out.println(cat1.getVoice());
		cat1.eat();

		System.out.println(cat2);
		System.out.println(cat2.getVoice());
		cat2.eat();

		System.out.println(dog1);
		System.out.println(dog1.getVoice());
		dog1.eat();

		System.out.println(dog2);
		System.out.println(dog2.getVoice());
		dog2.eat();

		Veterinarian vet1 = new Veterinarian("Doctor House");
		System.out.println(vet1);
		vet1.treatment(cat1);
		vet1.treatment(dog3);
		vet1.treatment(cat3);
	}
}