package comparator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

class Main {
	public static void main(String[] args) {
		Random rand = new Random();
		List<Integer> numbers = new ArrayList<Integer>();
		for (int i = 0; i < 5; i++) {
			numbers.add(rand.nextInt(100));
		}
		System.out.println("Original List: " + numbers);

		Collections.sort(numbers, new PrimeNumberComparator());
		int result = numbers.get(0);
		System.out.println("Resulting number: " + result);
		if (isPrime(result)) {
			System.out.println("We have received the maximum prime number in the list");
		} else {
			System.out.println("We have received the minimum number in the list");
		}
	}

	private static boolean isPrime(int number) {
		if (number < 2) {
			return false;
		}
		for (int i = 2; i <= Math.sqrt(number); i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

	static class PrimeNumberComparator implements Comparator<Integer> {
		public int compare(Integer a, Integer b) {
			if (isPrime(a) && isPrime(b)) {
				return Integer.compare(b, a);
			} else if (isPrime(a)) {
				return -1;
			} else if (isPrime(b)) {
				return 1;
			} else {
				return Integer.compare(a, b);
			}
		}
	}
}
