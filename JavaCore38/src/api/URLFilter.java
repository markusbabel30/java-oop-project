package api;

import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class URLFilter {
	public static void main(String[] args) throws IOException {
		// Create a text file with 10 URLs
		try (PrintWriter writer = new PrintWriter("urls.txt")) {
			writer.println("https://www.example1.com");
			writer.println("https://www.example2.com");
			writer.println("https://www.example3.com");
			writer.println("https://www.example4.com");
			writer.println("https://www.example5.com");
			writer.println("https://www.example6.com");
			writer.println("https://www.example7.com");
			writer.println("https://www.example8.com");
			writer.println("https://www.example9.com");
			writer.println("https://www.example10.com");
		}

		// Read the file and filter out only available URLs using Stream API
		try (Stream<String> stream = Files.lines(Paths.get("urls.txt"))) {
			List<String> availableUrls = stream.filter(URLFilter::isUrlAvailable).collect(Collectors.toList());

			System.out.println(availableUrls);
			// write the filtered urls to a new file
			Files.write(Paths.get("available_urls.txt"), availableUrls);
		}
	}

	private static boolean isUrlAvailable(String urlString) {
		try {
			URL url = new URL(urlString);
			if (!"http".equals(url.getProtocol()) && !"https".equals(url.getProtocol())) {
				return false;
			}
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("HEAD");
			int responseCode = connection.getResponseCode();
			return (responseCode == HttpURLConnection.HTTP_OK);
		} catch (IOException e) {
			return false;
		}
	}
}
