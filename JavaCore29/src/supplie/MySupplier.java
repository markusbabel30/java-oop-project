package supplie;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class MySupplier implements Supplier<String> {
	private List<String> list;
	private Predicate<String> predicate;
	private int currentIndex;

	public MySupplier(List<String> list, Predicate<String> predicate) {
		this.list = list;
		this.predicate = predicate;
		this.currentIndex = 0;
	}

	@Override
	public String get() {
		while (currentIndex < list.size()) {
			String current = list.get(currentIndex);
			currentIndex++;
			if (predicate.test(current)) {
				return current;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		List<String> list = Arrays.asList("Hello", "world", "Java");
		Predicate<String> startsWithUpperCase = s -> Character.isUpperCase(s.charAt(0));
		MySupplier supplier = new MySupplier(list, startsWithUpperCase);
		String next;
		while ((next = supplier.get()) != null) {
			System.out.println(next);
		}
	}
}
