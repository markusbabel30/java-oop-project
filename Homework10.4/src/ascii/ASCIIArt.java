package ascii;

import java.io.File;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.HashMap;

public class ASCIIArt {
  public static void main(String[] args) throws Exception {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter text:");
    String text = scanner.nextLine();

    Map<Character, String[]> asciiArtMap = new HashMap<>();
    asciiArtMap.put('A', new String[] { "  *   ", " * *  ", "*   * ", "*****", "*   * ", "*   * " });
    asciiArtMap.put('a', new String[] { "  *   ", " * *  ", "*   * ", "*****", "*   * ", "*   * " });
    asciiArtMap.put('B', new String[] { "****  ", "*   * ", "****  ", "*   * ", "*   * ", "****  " });
    asciiArtMap.put('b', new String[] { "****  ", "*   * ", "****  ", "*   * ", "*   * ", "****  " });
    asciiArtMap.put('C', new String[] { "  ***  ", " *   * ", "*     *", "*     *", " *   * ", "  ***  " });
    asciiArtMap.put('c', new String[] { "   *   ", "  *    ", " *     ", " *     ", "  *    ", "   *   " });
    asciiArtMap.put('D', new String[] { "****   ", "*   *  ", "*   *  ", "*   *  ", "*   *  ", "****   " });
    asciiArtMap.put('d', new String[] { "****   ", "*   *  ", "*   *  ", "*   *  ", "*   *  ", "****   " });
    asciiArtMap.put('E', new String[] { "***** ", "*     ", "*     ", "****  ", "*     ", "***** " });
    asciiArtMap.put('e', new String[] { "***** ", "*     ", "*     ", "****  ", "*     ", "***** " });
    asciiArtMap.put('F', new String[] { "***** ", "*     ", "*     ", "****  ", "*     ", "*     " });
    asciiArtMap.put('f', new String[] { "***** ", "*     ", "*     ", "****  ", "*     ", "*     " });
    asciiArtMap.put('G', new String[] { "  ***  ", " *   * ", "*     *", "*  *** ", " *   * ", "  ***  " });
    asciiArtMap.put('g', new String[] { "   *** ", "  *   *", " *     *", "  *****", "      *", "  ***  *" });
    asciiArtMap.put('H', new String[] { "*   * ", "*   * ", "*   * ", "***** ", "*   * ", "*   * " });
    asciiArtMap.put('h', new String[] { "*   * ", "*   * ", "*   * ", "***** ", "*   * ", "*   * " });
    asciiArtMap.put('I', new String[] { "*****", "  *  ", "  *  ", "  *  ", "  *  ", "*****" });
    asciiArtMap.put('i', new String[] { "*****", "  *  ", "  *  ", "  *  ", "  *  ", "*****" });
    asciiArtMap.put('J', new String[] { "*****", "   *  ", "   *  ", "   *  ", "*  *  ", "**    " });
    asciiArtMap.put('j', new String[] { "*****", "   *  ", "   *  ", "   *  ", "*  *  ", "**    " });
    asciiArtMap.put('K', new String[] { "*   * ", "*  *  ", "* *   ", "**    ", "* *   ", "*  *  " });
    asciiArtMap.put('k', new String[] { "*   * ", "*  *  ", "* *   ", "**    ", "* *   ", "*  *  " });
    asciiArtMap.put('L', new String[] { "*     ", "*     ", "*     ", "*     ", "*     ", "***** " });
    asciiArtMap.put('l', new String[] { "*     ", "*     ", "*     ", "*     ", "*     ", "***** " });
    asciiArtMap.put('M', new String[] { "*   * ", "** ** ", "* * * ", "*   * ", "*   * ", "*   * " });
    asciiArtMap.put('m', new String[] { "*   * ", "** ** ", "* * * ", "*   * ", "*   * ", "*   * " });
    asciiArtMap.put('N', new String[] { "*   * ", "**  * ", "* * * ", "*  ** ", "*   * ", "*   * " });
    asciiArtMap.put('n', new String[] { "*   * ", "**  * ", "* * * ", "*  ** ", "*   * ", "*   * " });
    asciiArtMap.put('O', new String[] { " ***  ", "*   * ", "*   * ", "*   * ", "*   * ", " ***  " });
    asciiArtMap.put('o', new String[] { " ***  ", "*   * ", "*   * ", "*   * ", "*   * ", " ***  " });
    asciiArtMap.put('P', new String[] { "****  ", "*   * ", "*   * ", "****  ", "*     ", "*     " });
    asciiArtMap.put('p', new String[] { "****  ", "*   * ", "*   * ", "****  ", "*     ", "*     " });
    asciiArtMap.put('Q', new String[] { " ***  ", "*   * ", "*   * ", "* * * ", " *   *", "  *** " });
    asciiArtMap.put('q', new String[] { " ***  ", "*   * ", "*   * ", "* * * ", " *   *", "  *** " });
    asciiArtMap.put('R', new String[] { "****  ", "*   * ", "*   * ", "****  ", "*  *  ", "*   * " });
    asciiArtMap.put('r', new String[] { "****  ", "*   * ", "*   * ", "****  ", "*  *  ", "*   * " });
    asciiArtMap.put('S', new String[] { " **** ", "*     ", "*     ", " ***  ", "     *", "****  " });
    asciiArtMap.put('s', new String[] { " **** ", "*     ", "*     ", " ***  ", "     *", "****  " });
    asciiArtMap.put('T', new String[] { "*****", "  *  ", "  *  ", "  *  ", "  *  ", "  *  " });
    asciiArtMap.put('t', new String[] { "*****", "  *  ", "  *  ", "  *  ", "  *  ", "  *  " });
    asciiArtMap.put('U', new String[] { "*   *", "*   *", "*   *", "*   *", "*   *", " *** " });
    asciiArtMap.put('u', new String[] { "*   *", "*   *", "*   *", "*   *", "*   *", " *** " });
    asciiArtMap.put('V', new String[] { "*   *", "*   *", "*   *", " * * ", "  *  ", "  *  " });
    asciiArtMap.put('v', new String[] { "*   *", "*   *", "*   *", " * * ", "  *  ", "  *  " });
    asciiArtMap.put('W', new String[] { "*   *", "*   *", "*   *", "* * *", "** **", "*   *" });
    asciiArtMap.put('w', new String[] { "*   *", "*   *", "*   *", "* * *", "** **", "*   *" });
    asciiArtMap.put('X', new String[] { "*   *", " * * ", "  *  ", " * * ", "*   *", "*   *" });
    asciiArtMap.put('x', new String[] { "*   *", " * * ", "  *  ", " * * ", "*   *", "*   *" });
    asciiArtMap.put('Y', new String[] { "*   *", " * * ", "  *  ", "  *  ", "  *  ", "  *  " });
    asciiArtMap.put('y', new String[] { "*   *", " * * ", "  *  ", "  *  ", "  *  ", "  *  " });
    asciiArtMap.put('Z', new String[] { "*****", "   * ", "  *  ", " *   ", "*    ", "*****" });
    asciiArtMap.put('z', new String[] { "*****", "   * ", "  *  ", " *   ", "*    ", "*****" });

    PrintWriter writer = new PrintWriter(new File("ascii_art.txt"));

    for (int i = 0; i < text.length(); i++) {
      char c = Character.toLowerCase(text.charAt(i));
      String[] asciiArt = asciiArtMap.get(c);
      if (asciiArt != null) {
        for (String s : asciiArt) {
          System.out.println(s);
          writer.println(s);
        }
      } else {
        System.out.println(c);
        writer.println(c);
      }
    }

    writer.close();
  }
}
