package api;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		String[] dependencies = {
				"<dependency>\n<groupId>junit</groupId>\n<artifactId>junit</artifactId>\n<version>4.4</version>\n<scope>test</scope>\n</dependency>",
				"<dependency>\n<groupId>org.powermock</groupId>\n<artifactId>powermock-reflect</artifactId>\n<version>3.2</version>\n</dependency>" };

		List<String> groupIds = Arrays.stream(dependencies).map(s -> s.split("\n")).flatMap(Arrays::stream)
				.filter(s -> s.contains("<groupId>")).map(s -> s.replace("<groupId>", "").replace("</groupId>", ""))
				.collect(Collectors.toList());

		System.out.println(groupIds);
	}
}
