package binaryoperator;

import java.util.function.BinaryOperator;

public class LongestWordOperator implements BinaryOperator<String> {
	@Override
	public String apply(String s1, String s2) {
		String[] words1 = s1.split(" ");
		String[] words2 = s2.split(" ");

		int maxLength = 0;
		String longestWord = "";
		for (String word : words1) {
			if (word.length() > maxLength) {
				maxLength = word.length();
				longestWord = word;
			}
		}
		for (String word : words2) {
			if (word.length() > maxLength) {
				maxLength = word.length();
				longestWord = word;
			}
		}
		return longestWord;
	}
}