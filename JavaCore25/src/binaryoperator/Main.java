package binaryoperator;

public class Main {
	public static void main(String[] args) {
		LongestWordOperator operator = new LongestWordOperator();
		String s1 = "The quick brown fox jumps over the lazy dog";
		String s2 = "Once upon a time in a galaxy far far away";
		String longestWord = operator.apply(s1, s2);
		System.out.println("The longest word is: " + longestWord);
	}
}
