package intpredicate;

import java.util.Scanner;
import java.util.function.IntPredicate;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int num = scanner.nextInt();

		IntPredicate evenSumPredicate = (int number) -> {
			int sum = 0;
			while (number > 0) {
				sum += number % 10;
				number /= 10;
			}
			return sum % 2 == 0;
		};

		boolean result = evenSumPredicate.test(num);
		System.out.println("The number you entered is: " + num);
		if (result) {
			System.out.println("The sum of digits of " + num + " is even.");
		} else {
			System.out.println("The sum of digits of " + num + " is odd.");
		}
	}
}
