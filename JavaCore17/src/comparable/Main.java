package comparable;

class Cat implements Comparable<Cat> {
	private String name;
	private int age;

	public Cat(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	@Override
	public int compareTo(Cat other) {
		return Integer.compare(this.age, other.age);
	}
}

public class Main {
	public static <T extends Comparable> T max(T[] array) {
		T max = array[0];
		for (T item : array) {
			if (item.compareTo(max) > 0) {
				max = item;
			}
		}
		return max;
	}

	public static void main(String[] args) {
		Cat[] cats = new Cat[] { new Cat("Whiskers", 3), new Cat("Bella", 5), new Cat("Luna", 2), new Cat("Max", 7),
				new Cat("Oliver", 1), new Cat("Charlie", 4), new Cat("Simba", 6), new Cat("Mittens", 8),
				new Cat("Socks", 9), new Cat("Fluffy", 10) };

		Cat oldestCat = max(cats);
		System.out.println("Oldest cat is " + oldestCat.getName() + " and it's age is " + oldestCat.getAge());
	}
}
