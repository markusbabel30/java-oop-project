package predicate;

import java.util.ArrayList;
import java.util.List;

public class Example {

	private List<String> names;

	public Example() {
		this.names = new ArrayList<>(List.of("Agata", "Sharon", "Evelyn", "Sophia", "Heather", "Tiffany", "Elizabeth", "Teresa"));
	}

	public void printOriginalList() {
		System.out.println("Original list:" + System.lineSeparator() + names);
	}

	public void removeNames(Character[] firstLetters) {
		for (Character character : firstLetters) {
			names.removeIf(a -> a.toLowerCase().startsWith(character.toString()));
		}
	}

	public void printModifiedList() {
		System.out.println("Modified list:" + System.lineSeparator() +names);
	}

	public static void main(String[] args) {
		Example main = new Example();
		main.printOriginalList();

		Character[] firstLetters = new Character[] { 'h', 'a', 't' };
		main.removeNames(firstLetters);

		main.printModifiedList();
	}
}
