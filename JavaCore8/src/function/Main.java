package function;

import java.util.Scanner;
import java.util.function.Function;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a string: ");
		String input = scanner.nextLine();

		Function<String, Integer> sumCodePoints = s -> s.codePoints().sum();

		System.out.println(
				"The sum of the code points of the characters in the string is: " + sumCodePoints.apply(input));
	}
}
