package filecopy;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class FileCopy {
    // Block size for the copy operation
    public static final int BLOCK_SIZE = 1024 * 1024; // 1 MB

    public static void main(String[] args) throws IOException {
        // Read the source and target file names from the standard input
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the source file name: ");
        String sourceFile = scanner.nextLine();
        System.out.print("Enter the target file name: ");
        String targetFile = scanner.nextLine();
        scanner.close();

        // Open the source and target files
        try (InputStream input = new BufferedInputStream(new FileInputStream(sourceFile));
             OutputStream output = new BufferedOutputStream(new FileOutputStream(targetFile))) {
            // Get the size of the source file
            long fileSize = input.available();

            // Start the copy operation in a separate thread
            CopyThread copyThread = new CopyThread(input, output, fileSize);
            new Thread(copyThread).start();

            // Show the progress of the copy operation
            while (copyThread.isRunning()) {
                long copied = copyThread.getCopied();
                double progress = (double) copied / fileSize;
                System.out.printf("\rProgress: %.2f%%", progress * 100);
                try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }

        System.out.println("\nFile copied successfully.");
    }
}

class CopyThread implements Runnable {
    private InputStream input;
    private OutputStream output;
    private long fileSize;
    private boolean running;
    private long copied;

    public CopyThread(InputStream input, OutputStream output, long fileSize) {
        this.input = input;
        this.output = output;
        this.fileSize = fileSize;
        this.running = true;
        this.copied = 0;
    }

    public boolean isRunning() {
        return running;
    }

    public long getCopied() {
        return copied;
    }

    @Override
    public void run() {
        try {
            // Allocate a buffer for the copy operation
            byte[] buffer = new byte[FileCopy.BLOCK_SIZE];

            // Read and write blocks until the end of the file is reached
            int count;
            while ((count = input.read(buffer)) != -1) {
                output.write(buffer, 0, count);
                copied += count;
            }

            running = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
