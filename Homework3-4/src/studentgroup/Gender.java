package studentgroup;

public enum Gender {
    MALE, FEMALE, NONBINARY
}
