package sample;

import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String, String> map1 = new HashMap<>();

		map1.put("h", "hello");
		map1.put("n", "new");
		map1.put("y", "year");

		System.out.println(map1);

		String temp = map1.get("h");
		System.out.println(temp);

		map1.put("n", "New");

		System.out.println(map1);
		map1.remove("n");
		System.out.println(map1);

		for (String k : map1.keySet()) {
			System.out.println(k + "   " + map1.get(k));
		}
	}

}