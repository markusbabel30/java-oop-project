package api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class Main {
	public static void main(String[] args) throws IOException {
		String directory = "/path/to/directory";
		Path path = Paths.get(directory);
		Optional<Path> maxSizeFile = Files.list(path).filter(p -> !Files.isDirectory(p))
				.max((p1, p2) -> Long.compare(p1.toFile().length(), p2.toFile().length()));
		if (maxSizeFile.isPresent()) {
			System.out.println(maxSizeFile.get().toAbsolutePath());
		}
	}
}
