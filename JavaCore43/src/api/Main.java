package api;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		Random rand = new Random();
		for (int i = 0; i < 20; i++) {
			list.add(rand.nextInt(31));
		}
		System.out.println("Original list: " + list);
		list.stream().filter(i -> i > 10).sorted((a, b) -> (a % 10) - (b % 10)).forEach(System.out::println);
	}
}
