package sample;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class FileIdentityService {

	public static boolean areFilesIdentical(String file1, String file2) throws IOException {

		FileInputStream file1Stream = new FileInputStream(file1);
		FileInputStream file2Stream = new FileInputStream(file2);

		byte[] file1Contents = readInputStream(file1Stream);
		byte[] file2Contents = readInputStream(file2Stream);

		return Arrays.equals(file1Contents, file2Contents);
	}

	private static byte[] readInputStream(FileInputStream inputStream) throws IOException {
		byte[] buffer = new byte[8192];
		int bytesRead;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
		return output.toByteArray();
	}

}
