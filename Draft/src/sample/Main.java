package sample;

import java.io.IOException;
import java.util.Scanner;

public class Main {

	  public static void main(String[] args) throws IOException {
	        Scanner scanner = new Scanner(System.in);
	        System.out.print("Enter the file path for the first file: ");
	        String file1 = scanner.nextLine();
	        System.out.print("Enter the file path for the second file: ");
	        String file2 = scanner.nextLine();

	        if (FileIdentityService.areFilesIdentical(file1, file2)) {
	            System.out.println("The files are identical.");
	        } else {
	            System.out.println("The files are different.");
	        }
	    }
	}

