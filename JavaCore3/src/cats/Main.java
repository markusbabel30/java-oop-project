package cats;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {

	public static void removeCats(List<Cat> cats, int age, char name) {
		Predicate<Cat> agePredicate = c -> c.getAge() < age;
		Predicate<Cat> namePredicate = c -> c.getName().charAt(0) > name;
		cats.removeIf(agePredicate.and(namePredicate));
	}

	public static void main(String[] args) {
		Cat cat1 = new Cat("Umka", 12);
		Cat cat2 = new Cat("Luska", 5);
		Cat cat3 = new Cat("Barsic", 8);
		Cat cat4 = new Cat("Timka", 4);
		Cat cat5 = new Cat("Kuzia", 2);
		List<Cat> cats = new ArrayList<>(List.of(cat1, cat2, cat3, cat4, cat5));

		System.out.println("Original list: " + cats);
		removeCats(cats, 5, 'C');
		System.out.println("Modified list: " + cats);
	}

}

class Cat {
	private String name;
	private int age;

	public Cat(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + ", age=" + age + "]";
	}
}
