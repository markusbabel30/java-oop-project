package bipredicate;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;

public class Main {
	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<>();
		map.put(3, "Hello");
		map.put(4, "Java");
		map.put(5, "world");
		map.put(6, "example");
		map.put(7, "program");
		map.put(8, "removeIf");
		map.put(9, "BiPredicate");
		map.put(10, "Map");
		map.put(11, "KeyValue");
		map.put(12, "StringLength");

		System.out.println("Map before removing: " + map);

		BiPredicate<Integer, String> biPredicate = (key, value) -> key != value.length();
		map.entrySet().removeIf(entry -> biPredicate.test(entry.getKey(), entry.getValue()));

		System.out.println("Map after removing: " + map);
	}
}
