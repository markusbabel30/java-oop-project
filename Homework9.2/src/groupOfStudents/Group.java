package groupOfStudents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Group {
	private List<Student> students = new ArrayList<>();
	private String groupName;

	public Group(String groupName, List<Student> students) {
		this.groupName = groupName;
		this.students = students;
	}

	public Group(String groupName) {
		this.groupName = groupName;
	}

	public Group() {
	}

	public static void sortStudentsByLastName(List<Student> students) {
		students.sort(Comparator.nullsLast(new StudentLastNameComparator()));
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public void addStudent(Student student) throws GroupOverflowException {
	    if (students.size() >= 10) {
	        throw new GroupOverflowException("The group already has the maximum number of students (10)");
	    }
	    student.setGroupName(this.groupName);
	    this.students.add(student);
	    System.out.println("\u001B[32m" + student.getName() + " " + student.getLastName() + " added to "
	        + this.groupName + " group." + "\u001B[0m");
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (Student student : this.students) {
			if (student.getLastName().equals(lastName)) {
				return student;
			}
		}
		throw new StudentNotFoundException(
				"Student with last name " + lastName + " not found " + this.groupName + " group.");
	}

	public boolean removeStudentByID(int id) {
		for (Student student : this.students) {
			if (student.getId() == id) {
				this.students.remove(student);
				System.out.println("Student with student card id " + id + " is removed from group " + this.groupName);
				return true;
			}
		}
		return false;
	}

	static void printEquivalentStudents(Group group) {
		List<Student> students = group.getStudents();
		for (int i = 0; i < students.size(); i++) {
			Student student = students.get(i);
			for (int j = i + 1; j < students.size(); j++) {
				Student otherStudent = students.get(j);
				if (student.getName().equals(otherStudent.getName())
						&& student.getLastName().equals(otherStudent.getLastName())) {
					System.out.println(student + " and " + otherStudent + " are equivalent students.");
				}
			}
		}
	}

	@Override
	public String toString() {
		return "Group [students=" + students + ", groupName=" + groupName + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(groupName, students);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		return Objects.equals(groupName, other.groupName) && Objects.equals(students, other.students);
	}

}
