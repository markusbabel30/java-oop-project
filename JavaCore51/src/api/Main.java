package api;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
	public static void main(String[] args) {
		Random rand = new Random();
		// Generate a list of 20 random integers between 0 and 100
		List<Integer> numbers = IntStream.range(0, 20).map(i -> rand.nextInt(101)).boxed().collect(Collectors.toList());
		System.out.println("Original List: " + numbers);
		// Filter the list and collect even numbers in a string separated by ;
		String evenNumbers = numbers.stream().filter(n -> n % 2 == 0).map(String::valueOf)
				.collect(Collectors.joining(";"));

		// Filter the list and collect odd numbers in a string separated by ;
		String oddNumbers = numbers.stream().filter(n -> n % 2 != 0).map(String::valueOf)
				.collect(Collectors.joining(";"));

		System.out.println("Even Numbers: " + evenNumbers);
		System.out.println("Odd Numbers: " + oddNumbers);
	}
}
