package doubleCola;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class DoubleCola {
	public static void main(String[] args) {
		// Create a queue to hold the names of the people in line
		Queue<String> queue = new LinkedList<>();
		queue.add("Sheldon");
		queue.add("Leonard");
		queue.add("Wolowitz");
		queue.add("Koothrappally");
		queue.add("Penny");

		// Read the number of glasses of cola issued from the scanner
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the number of glasses of cola issued: ");
		int glasses = scanner.nextInt();

		// Split the people in the queue according to the number of glasses of cola
		// issued
		for (int i = 0; i < glasses; i++) {
			String person = queue.poll();
			queue.add(person);
			queue.add(person);
		}

		// Print the final queue
		System.out.println("Queue: " + queue);
	}
}
