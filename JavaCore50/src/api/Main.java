package api;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		String input = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
		List<String> list = Pattern.compile("[^\\p{L}\\p{Nd}']+").splitAsStream(input.toLowerCase())
				.collect(Collectors.toList());
		Map<String, Map<Character, Long>> result = list.stream().filter(word -> word.matches(".*[aeiou].*"))
				.collect(Collectors.toMap(Function.identity(), word -> {
					Map<Character, Long> vowels = new HashMap<>();
					for (char c : word.toCharArray()) {
						if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
							vowels.merge(c, 1L, Long::sum);
						}
					}
					return vowels;
				}));

		System.out.println(result);
	}
}
