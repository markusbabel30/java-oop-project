package consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class StringConsumer implements Consumer<String> {
	private List<String> numbersList = new ArrayList<>();

	public void accept(String s) {
		if (s.matches(".*\\d+.*")) {
			numbersList.add(s);
		}
	}

	public List<String> getNumbersList() {
		return numbersList;
	}

	public static void main(String[] args) {
		List<String> stringsList = new ArrayList<>();
		stringsList.add("Hello");
		stringsList.add("123");
		stringsList.add("Goodbye");
		stringsList.add("456");
		stringsList.add("Foo");
		stringsList.add("789");
		stringsList.add("Bar");
		stringsList.add("012");
		stringsList.add("Baz");
		stringsList.add("345");

		StringConsumer consumer = new StringConsumer();
		stringsList.forEach(consumer);
		System.out.println("Original List: " + stringsList);
		System.out.println("List with numbers: " + consumer.getNumbersList());
	}
}
