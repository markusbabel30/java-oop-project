package api;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class Main {
	public static void main(String[] args) {
		List<String> words = Arrays.asList("apple", "banana", "orange", "apple", "banana", "orange");

		Set<String> uniqueWords = words.stream().collect(MyCollectors.toSet(HashSet::new));
		System.out.println("Unique words: " + uniqueWords);
	}

	static class MyCollectors {
		public static <T> Collector<T, ?, Set<T>> toSet(Supplier<Set<T>> setSupplier) {
			return Collector.of(setSupplier, Set::add, (left, right) -> {
				left.addAll(right);
				return left;
			}, Collector.Characteristics.IDENTITY_FINISH);
		}
	}
}
