package api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) {
		Path path = Paths.get("path/to/file.txt");
		try (Stream<String> lines = Files.lines(path)) {
			lines.filter(line -> {
				try {
					return Files.list(Paths.get(line)).filter(p -> p.toString().endsWith(".txt")).count() > 3;
				} catch (IOException e) {
					e.printStackTrace();
				}
				return false;
			}).findFirst().ifPresent(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}