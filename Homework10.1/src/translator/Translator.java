package translator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Translator {
	private Map<String, String> dictionary;

	public Translator() {
		this.dictionary = new HashMap<>();
		this.populateDictionary();
	}

	private void populateDictionary() {
		dictionary.put("hello", "привіт");
		dictionary.put("world", "світ");
		dictionary.put("dog", "собака");
		dictionary.put("cat", "кіт");
		/*
		 * Modifying the dictionary for the translation of the haiku:
		 * Golden autumn leaves
		 * Drifting gently to the ground
		 * Nature's tapestry
		 */
		dictionary.put("golden", "золоте");
		dictionary.put("autumn", "осінне");
		dictionary.put("leaves", "листя");
		dictionary.put("drifting", "дрейфує");
		dictionary.put("gently", "м'яко");
		dictionary.put("to", "до");
		dictionary.put("the", "самої");
		dictionary.put("ground", "землі");
		dictionary.put("nature's", "природній");
		dictionary.put("tapestry", "гобелен");
	}

	public void translate(String inputFile, String outputFile) throws IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));
				BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
			String line;
			StringBuilder sb = new StringBuilder();
			// Translate each line and append it to the StringBuilder
			while ((line = reader.readLine()) != null) {
				String[] words = line.split("\\s+");
				for (String word : words) {
					// Convert the word to lowercase before looking it up in the dictionary
					String lowercaseWord = word.toLowerCase();
					String translation = dictionary.get(lowercaseWord);
					if (translation != null) {
						sb.append(translation);
					} else {
						sb.append(word);
					}
					sb.append(" ");
				}
				sb.append("\n");
			}
			// Write the translated text to the output file and console
			String translatedText = sb.toString();
			writer.write(translatedText);
			System.out.println(translatedText);
		}
	}

	public static void main(String[] args) throws IOException {
		Translator translator = new Translator();
		translator.translate("English.in.txt", "Ukrainian.out.txt");
	}
}
