package binaryoperator;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.BinaryOperator;

public class ListIntersection {
	public static void main(String[] args) {
		BinaryOperator<List<Integer>> operator = (list1, list2) -> {
			list1.retainAll(list2);
			return list1;
		};

		List<Integer> list1 = new ArrayList<Integer>();
		list1.addAll(Arrays.asList(1, 2, 3, 4, 5, 6));
		List<Integer> list2 = new ArrayList<Integer>();
		list2.addAll(Arrays.asList(4, 5, 6, 7, 8, 9));

		List<Integer> result = operator.apply(list1, list2);
		System.out.println(result);
	}
}
