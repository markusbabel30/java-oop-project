package factorialthreads;

import java.math.BigInteger;

public class FactorialThreads {
	 public static void main(String[] args) {
	        // Create 100 threads
	        for (int i = 1; i <= 100; i++) {
	            final int threadNumber = i;
	            Thread thread = new Thread(() -> {
	                // Calculate the factorial of the thread number
	                BigInteger factorial = BigInteger.ONE;
	                for (int j = 2; j <= threadNumber; j++) {
	                    factorial = factorial.multiply(BigInteger.valueOf(j));
	                }

	                // Print the result to the console
	                System.out.println("Thread " + threadNumber + ": " + threadNumber + "! = " + factorial);
	            });

	            // Start the thread
	            thread.start();
	        }
	    }
	}
