package leapyear;

import java.util.Scanner;

public class DayCounter {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a year: ");
		int year = sc.nextInt();
		sc.close();

		if (year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0) {
					System.out.println(year + " is a leap year. It has 366 days.");
				} else {
					System.out.println(year + " is not a leap year. It has 365 days.");
				}
			} else {
				System.out.println(year + " is a leap year. It has 366 days.");
			}
		} else {
			System.out.println(year + " is not a leap year. It has 365 days.");
		}
	}
}
