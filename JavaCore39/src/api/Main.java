package api;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Singer {
	private String name;
	private String[] songs;

	public Singer(String name, String[] songs) {
		this.name = name;
		this.songs = songs;
	}

	public String[] getSongs() {
		return songs;
	}
}

public class Main {

	public static void main(String[] args) {

		Singer singer1 = new Singer("Freddie Mercury",
				new String[] { "We Are the Champions", "Somebody to Love", "Bohemian Rhapsody" });
		Singer singer2 = new Singer("David Bowie",
				new String[] { "Space Oddity", "Let Me Sleep Beside You", "Heroes" });
		Singer singer3 = new Singer("James Paul McCartney",
				new String[] { "Can’t Buy Me Love", "Another Girl", "Hey Jude" });
		Singer singer4 = new Singer("Elton John",
				new String[] { "Rocket Man", "Tiny Dancer", "Goodbye Yellow Brick Road" });
		Singer singer5 = new Singer("Bob Dylan",
				new String[] { "Blowin' in the Wind", "The Times They Are a-Changin'", "Like a Rolling Stone" });
		Singer singer6 = new Singer("Stevie Wonder",
				new String[] { "Superstition", "Signed, Sealed, Delivered", "Sir Duke" });
		Singer singer7 = new Singer("Led Zeppelin", new String[] { "Stairway to Heaven", "Kashmir", "Black Dog" });
		Singer singer8 = new Singer("Pink Floyd",
				new String[] { "Wish You Were Here", "Another Brick in the Wall", "Comfortably Numb" });
		Singer singer9 = new Singer("The Beatles", new String[] { "Hey Jude", "Let It Be", "Yesterday" });
		Singer singer10 = new Singer("The Rolling Stones",
				new String[] { "Satisfaction", "Brown Sugar", "Start Me Up" });
		Singer[] rockStars = new Singer[] { singer1, singer2, singer3, singer4, singer5, singer6, singer7, singer8,
				singer9, singer10 };

		List<String> firstThreeSongs = Arrays.stream(rockStars).flatMap(singer -> Arrays.stream(singer.getSongs()))
				.sorted().limit(3).collect(Collectors.toList());
		System.out.println(firstThreeSongs);
	}
}
