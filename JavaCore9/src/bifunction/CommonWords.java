package bifunction;

import java.util.Arrays;
import java.util.function.BiFunction;

public class CommonWords implements BiFunction<String, String, String[]> {
	@Override
	public String[] apply(String text1, String text2) {
		String[] words1 = text1.split(" ");
		String[] words2 = text2.split(" ");
		return Arrays.stream(words1).filter(word -> Arrays.asList(words2).contains(word)).toArray(String[]::new);
	}

	public static void main(String[] args) {
		CommonWords commonWords = new CommonWords();
		String text1 = "Hello Java";
		String text2 = "Hello Python";
		String[] result = commonWords.apply(text1, text2);
		System.out.println(Arrays.toString(result));
	}
}
