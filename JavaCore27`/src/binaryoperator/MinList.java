package binaryoperator;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;

public class MinList<T extends Comparable<T>> implements BinaryOperator<List<T>> {

	@Override
	public List<T> apply(List<T> list1, List<T> list2) {
		T min1 = list1.stream().min(T::compareTo).get();
		T min2 = list2.stream().min(T::compareTo).get();
		if (min1.compareTo(min2) <= 0) {
			return list1;
		} else {
			return list2;
		}
	}

	public static void main(String[] args) {
		MinList<Integer> minList = new MinList<>();
		List<Integer> list1 = Arrays.asList(5, 0, 3, 4);
		List<Integer> list2 = Arrays.asList(10, -2, 5);
		List<Integer> min = minList.apply(list1, list2);
		System.out.println(min);
	}
}
