package supplier;

import java.util.function.Supplier;

class WordSupplier implements Supplier<String> {
	private String[] words;
	private int index;

	public WordSupplier(String input) {
		this.words = input.split(" ");
		this.index = 0;
	}

	@Override
	public String get() {
		if (index < words.length) {
			return words[index++];
		} else {
			return null;
		}
	}
}

public class Main {
	public static void main(String[] args) {
		WordSupplier supplier = new WordSupplier(
				"Shall I compare thee to a summer's day? Thou art more lovely and more temperate.");
		String word;
		while ((word = supplier.get()) != null) {
			System.out.println(word);
		}
	}
}
