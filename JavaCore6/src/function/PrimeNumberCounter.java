package function;

import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.Arrays;

public class PrimeNumberCounter {
	public static void main(String[] args) {
		Random rand = new Random();
		Integer[] numbers = new Integer[20];
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = rand.nextInt(100);
		}
		System.out.println("Generated Array: " + Arrays.toString(numbers));
		Function<Integer[], Integer> primeCounter = arr -> {
			int count = 0;
			Predicate<Integer> isPrime = num -> {
				if (num < 2)
					return false;
				for (int i = 2; i <= Math.sqrt(num); i++) {
					if (num % i == 0)
						return false;
				}
				return true;
			};
			for (Integer number : arr) {
				if (isPrime.test(number))
					count++;
			}
			return count;
		};
		int result = primeCounter.apply(numbers);
		System.out.println("The number of prime numbers in the array is: " + result);
	}
}
