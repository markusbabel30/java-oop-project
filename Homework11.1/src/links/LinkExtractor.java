package links;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkExtractor {

	private static final String URL_REGEX = "http(s)?://[\\w-]+(\\.[\\w-]+)+([\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-])?";

	public static List<String> extractLinks(String urlString) throws Exception {
		URL url = new URL(urlString);
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		String line;
		List<String> links = new ArrayList<>();
		Pattern linkPattern = Pattern.compile(URL_REGEX);
		while ((line = reader.readLine()) != null) {
			Matcher matcher = linkPattern.matcher(line);
			while (matcher.find()) {
				String link = matcher.group();
				links.add(link);
			}
		}
		reader.close();
		return links;
	}

	public static void main(String[] args) throws Exception {
		String urlString = "https://www.example.com";
		List<String> links = extractLinks(urlString);
		FileWriter writer = new FileWriter("links.txt");
		for (String link : links) {
			System.out.println(link);
			writer.write(link + "\n");
		}
		writer.close();
	}
}
