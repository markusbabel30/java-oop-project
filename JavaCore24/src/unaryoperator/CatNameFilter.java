package unaryoperator;

import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class CatNameFilter implements UnaryOperator<List<String>> {
	public List<String> apply(List<String> list) {
		return list.stream().filter(s -> s.length() > 5).collect(Collectors.toList());
	}

	public static void main(String[] args) {
		List<String> catNames = Arrays.asList("Whiskers", "Fluffy", "Simba", "Tiger", "Luigi", "Mittens", "Smokey",
				"Bella", "Oliver", "Jasp");
		CatNameFilter filter = new CatNameFilter();
		List<String> filteredList = filter.apply(catNames);
		System.out.println("Original List: " + catNames);
		System.out.println("Filtered List: " + filteredList);
	}
}
