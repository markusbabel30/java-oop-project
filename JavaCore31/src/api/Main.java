package api;

import java.util.ArrayList;
import java.util.List;

class Cat {
	String name;
	int age;
	String color;

	public Cat(String name, int age, String color) {
		this.name = name;
		this.age = age;
		this.color = color;
	}
}

public class Main {
	public static void main(String[] args) {
		List<Cat> cats = new ArrayList<>();
		cats.add(new Cat("Whiskers", 2, "black"));
		cats.add(new Cat("Fluffy", 4, "white"));
		cats.add(new Cat("Simba", 3, "golden"));
		cats.add(new Cat("Mittens", 5, "gray"));
		cats.add(new Cat("Socks", 1, "brown"));

		String longestName = cats.stream().map(cat -> cat.name).max((name1, name2) -> name1.length() - name2.length())
				.orElse("");

		System.out.println("The cat with the longest name is: " + longestName);
	}
}
