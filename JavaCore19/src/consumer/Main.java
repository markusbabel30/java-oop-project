package consumer;

import java.util.List;
import java.util.function.Consumer;

public class Main {
	public static void main(String[] args) {
		List<Integer> numbers = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		Consumer<Integer> printOddNumbers = number -> {
			if (number % 2 != 0) {
				System.out.println(number);
			}
		};
		numbers.forEach(printOddNumbers);
	}
}
