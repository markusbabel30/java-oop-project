package unaryoperator;

import java.util.Scanner;
import java.util.function.UnaryOperator;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a string: ");
		String input = scanner.nextLine();

		UnaryOperator<String> filterNumeric = s -> s.replaceAll("[^\\d]", "");
		System.out.println("Result: " + filterNumeric.apply(input));
	}
}
