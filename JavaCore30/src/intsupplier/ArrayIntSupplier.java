package intsupplier;

import java.util.function.IntSupplier;
import java.util.Random;

public class ArrayIntSupplier implements IntSupplier {
    private final int[] array;
    private int index = 0;

    public ArrayIntSupplier(int[] array) {
        this.array = array;
    }

    public static int[] generateRandomArray(int size) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(101);
        }
        return array;
    }

    @Override
    public int getAsInt() {
        return array[index++];
    }

    public static void main(String[] args) {
        int[] arr = generateRandomArray(20);
        ArrayIntSupplier supplier = new ArrayIntSupplier(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(supplier.getAsInt());
        }
    }
}
