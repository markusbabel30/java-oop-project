package api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main {
	public static void main(String[] args) throws IOException {
		String directory = "path/to/directory";
		try (Stream<java.nio.file.Path> stream = Files.walk(Paths.get(directory))) {
			stream.filter(Files::isRegularFile).filter(path -> path.toString().endsWith(".txt"))
					.forEach(System.out::println);
		}
	}
}
