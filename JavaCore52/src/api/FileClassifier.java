package api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FileClassifier {
	public static void main(String[] args) {
		// Path to directory containing files
		Path dir = Path.of("path/to/directory");
		try {
			// Get list of files in directory
			List<Path> files = Files.list(dir).collect(Collectors.toList());
			// Group files by size range
			Map<String, List<Path>> groupedFiles = files.stream().collect(Collectors.groupingBy(f -> {
				long size = f.toFile().length();
				if (size < 1_000_000)
					return "0 - 1MB";
				if (size < 10_000_000)
					return "1MB - 10MB";
				if (size < 100_000_000)
					return "10MB - 100MB";
				if (size < 1_000_000_000)
					return "100MB - 1GB";
				return "1GB+";
			}));
			// Print groups of files in ascending order
			groupedFiles.entrySet().stream().sorted((e1, e2) -> {
				long size1 = e1.getValue().get(0).toFile().length();
				long size2 = e2.getValue().get(0).toFile().length();
				return Long.compare(size1, size2);
			}).forEach(e -> {
				System.out.println("Files in range " + e.getKey() + ":");
				e.getValue()
						.forEach(f -> System.out.println("\t" + f.toString() + " - " + f.toFile().length() + " bytes"));
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
