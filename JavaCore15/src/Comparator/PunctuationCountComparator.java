package Comparator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;

public class PunctuationCountComparator implements Comparator<File> {

	@Override
	public int compare(File file1, File file2) {
		int punctuationCount1 = countPunctuationMarks(file1);
		int punctuationCount2 = countPunctuationMarks(file2);
		return Integer.compare(punctuationCount1, punctuationCount2);
	}

	private static int countPunctuationMarks(File file) {
		int count = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = br.readLine()) != null) {
				for (int i = 0; i < line.length(); i++) {
					if (isPunctuationMark(line.charAt(i))) {
						count++;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return count;
	}

	private static boolean isPunctuationMark(char c) {
		return c == ',' || c == '.' || c == '-' || c == '?' || c == '!' || c == ' ';
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number of files you want to compare: ");
		int num = scanner.nextInt();
		File[] files = new File[num];
		scanner.nextLine();
		for (int i = 0; i < num; i++) {
			System.out.println("Enter the path of file " + (i + 1) + " : ");
			String path = scanner.nextLine();
			files[i] = new File(path);
		}
		Arrays.sort(files, new PunctuationCountComparator());
		System.out.println("Sorted files based on punctuation count: ");
		for (File file : files) {
			System.out.println(file.getName() + " - punctuation count: " + countPunctuationMarks(file));
		}
	}
}
