package sample;

import java.math.BigInteger;

public class FactorialCount implements Runnable {
	private int number;

	public FactorialCount(int number) {
		super();
		this.number=number;
	}

	public BigInteger calculate(int n) {
		BigInteger result = BigInteger.ONE;
		for (int i = 1; i <= n; i++) {
			result = result.multiply(BigInteger.valueOf(i));
		}
		return result;
	}

	@Override
	public void run() {
			Thread thr = Thread.currentThread();
			thr.setName("Thread - " + number);
			BigInteger fact = calculate(number);
			System.out.println(thr.getName() + ":\t " + number + "! = " + fact);
		
	}

}
