package translator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Translator {
    private Map<String, String> dictionary;

    public Translator() {
        this.dictionary = new HashMap<>();
    }

    public void loadDictionary(String dictionaryFile) throws IOException {
        try (BufferedReader dictionaryReader = new BufferedReader(new FileReader(dictionaryFile))) {
            String line;
            while ((line = dictionaryReader.readLine()) != null) {
                String[] entry = line.split("\\s+");
                dictionary.put(entry[0].toLowerCase(), entry[1]);
            }
        }
    }

    public void addDictionaryEntries() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter an English word and its Ukrainian translation, separated by a space (or press Enter to finish): ");
            String input = scanner.nextLine();
            if (input.isBlank()) {
                break;
            }
            String[] entry = input.split("\\s+");
            dictionary.put(entry[0].toLowerCase(), entry[1]);
        }
    }

    public void saveDictionary(String dictionaryFile) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dictionaryFile))) {
            for (Map.Entry<String, String> entry : dictionary.entrySet()) {
                writer.write(entry.getKey() + " " + entry.getValue() + "\n");
            }
        }
    }

    public void translate(String inputFile, String outputFile) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));
             BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFile))) {
            String line;
            StringBuilder sb = new StringBuilder();

            // Translate each line and append it to the StringBuilder
            while ((line = reader.readLine()) != null) {
                String[] words = line.split("\\s+");
                for (String word : words) {
                    // Convert the word to lowercase before looking it up in the dictionary
                    String lowercaseWord = word.toLowerCase();
                    String translation = dictionary.get(lowercaseWord);
                    if (translation != null) {
                        sb.append(translation);
                    } else {
                        sb.append(word);
                    }
                    sb.append(" ");
                }
                sb.append("\n");
            }

            // Write the translated text to the output file and console
            String translatedText = sb.toString();
            outputWriter.write(translatedText);
            System.out.println(translatedText);
        }
    }

    public static void main(String[] args) throws IOException {
        Translator translator = new Translator();
        translator.loadDictionary("dictionary.txt");
        translator.addDictionaryEntries();
        translator.saveDictionary("dictionary.txt");
        translator.translate("English.in.txt", "Ukrainian.out.txt");
    }
}
