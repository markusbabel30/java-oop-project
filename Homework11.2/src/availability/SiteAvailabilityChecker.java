package availability;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class SiteAvailabilityChecker {

	public static void main(String[] args) throws Exception {
		FileReader fileReader = new FileReader("sites.txt");
		BufferedReader reader = new BufferedReader(fileReader);
		FileWriter writer = new FileWriter("results.txt");
		String site;
		while ((site = reader.readLine()) != null) {
			URL url = new URL(site);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("HEAD");
			int responseCode = connection.getResponseCode();
			if (responseCode == 200) {
				System.out.println(site + " is available");
				writer.write(site + " is available\n");
			} else {
				System.out.println(site + " is not available");
				writer.write(site + " is not available\n");
			}
		}
		reader.close();
		writer.close();
	}
}
