package phonecall;

import java.util.Scanner;

public class Main {

	private static Scanner sc = new Scanner(System.in);
	private static Phone myNumber = new Phone();
	private static Network myTelecom = new Network();

	public static void main(String[] args) {
		for (;;) {
			System.out.println("Please enter your phone number (12-digit format):");
			String line = sc.nextLine();
			System.out.println();
			if (isCorrect(line)) {
				myNumber.setNumber(Long.valueOf(line));
				break;
			} else {
				System.out.println("Wrong entry! Mobile number must be a 12-digit number!");
			}
			System.out.println();
		}

		for (;;) {
			System.out.println("Please tell me, what do you want to do next?");
			System.out.println("register, call or quit?");
			String nextAction = sc.nextLine();
			System.out.println();

			if (nextAction.equals("quit")) {
				System.out.println("Have a nice day! Bye");
				break;

			} else if (nextAction.equals("register")) {
				newNumberRegistartion();

			} else if (nextAction.equals("call")) {
				outgoingcall();

			} else {
				System.out.println("Sorry, I didn't understand you!");
			}
		}
	}

	private static void outgoingcall() {
		for (;;) {
			System.out.println("Input number to call:");
			String input = sc.nextLine();
			System.out.println();

			if (isCorrect(input)) {
				long number = Long.valueOf(input);
				myNumber.calling(number);
				break;
			} else {
				System.out.println("Wrong entry! Mobile number must be a 12-digit number!" + System.lineSeparator());
				continue;
			}
		}
	}

	private static void newNumberRegistartion() {
		for (;;) {
			System.out.println("Please enter number you want to register:");
			String input = sc.nextLine();
			System.out.println();
			if (isCorrect(input)) {
				if (Long.valueOf(input) == myNumber.getNumber()) {
					myNumber.registerInNetwork(myTelecom);
					break;
				} else {
					Phone newNumber = new Phone(Long.valueOf(input));
					newNumber.registerInNetwork(myTelecom);
					System.out.println(myTelecom);
					break;
				}
			} else {
				System.out.println("Wrong entry! Mobile number must be a 12-digit number!");
			}
		}
	}

	public static boolean isCorrect(String str) {
		if (str == null || str.isEmpty() || str.length() != 12)
			return false;
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)))
				return false;
		}
		return true;
	}
}
