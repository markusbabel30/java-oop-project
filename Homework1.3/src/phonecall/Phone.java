package phonecall;

public class Phone {
	private long number;
	private Network network;

	public Phone(long number) {
		super();
		this.number = number;
	}

	public Phone() {
		super();
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}

	public void registerInNetwork(Network nwk) {
		this.setNetwork(nwk);
		nwk.registration(this);
	}

	public void calling(long recipient) {
		if (this.getNetwork() != null) {
			System.out.println("We are calling to abonent with number: " + recipient + System.lineSeparator());
			this.network.call(this, recipient);
		} else {
			System.out.println("You can't make a call because your own number " + this.number + System.lineSeparator()
					+ " is still not registered!" + System.lineSeparator());
		}
	}

	public void incomingCall(long incomingNumber) {
		if (this.number != incomingNumber) {
			System.out.println("Abonent " + this.number + " has incoming call from number: " + incomingNumber
					+ System.lineSeparator());
		} else {
			System.out.println("You can't call to yourself!" + System.lineSeparator());
		}
	}

	@Override
	public String toString() {
		return "Abonent [" + number + "]" + System.lineSeparator();
	}

}
