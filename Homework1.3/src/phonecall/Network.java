package phonecall;

import java.util.Arrays;
import java.util.Scanner;

public class Network {

	Scanner sc = new Scanner(System.in);
	private Phone[] dataBase = new Phone[1];
	private int i = 0;

	public Network(Phone[] dataBase) {
		super();
		this.dataBase = dataBase;
	}

	public Network() {
		super();
	}

	public Phone[] getDataBase() {
		return dataBase;
	}

	public void setDataBase(Phone[] dataBase) {
		this.dataBase = dataBase;
	}

	public void call(Phone myPhone, long number) {
		if (checkNumber(number)) {
			Phone recipient = this.findNumber(number);
			recipient.incomingCall(myPhone.getNumber());
		} else {
			System.out.println("You cannot make calls to unregistered phone." + System.lineSeparator());
		}
	}

	public Phone[] registration(Phone number) {

		if (checkNumber(number.getNumber())) {
			System.out.println("This phone number already in the system");
			System.out.println();
		} else {
			dataBase = Arrays.copyOf(dataBase, i + 1);
			dataBase[i] = number;
			i++;
			System.out.println("Phone number " + number.getNumber() + " successfully registered in the system");
			System.out.println();
		}

		return dataBase;

	}

	public boolean checkNumber(long number) {
		boolean result = false;
		for (int i = 0; i < dataBase.length; i++) {
			if (dataBase[i] != null) {

				if (number == dataBase[i].getNumber()) {
					result = true;
					break;
				}
			}
		}

		return result;
	}

	public Phone findNumber(long number) {
		Phone result = null;
		for (int i = 0; i < dataBase.length; i++) {
			if (dataBase[i].getNumber() == number) {
				result = dataBase[i];
				break;
			}
		}
		return result;
	}

	@Override

	public String toString() {
		String temp = "";
		for (Phone result : dataBase)
			if (result != null) {
				temp += result.toString();
			}
		return "Network: " + System.lineSeparator() + temp;
	}

}
