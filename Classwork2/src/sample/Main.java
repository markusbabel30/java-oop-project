package sample;

public class Main {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    Employee employee1 = new Employee("Alex", "Ts", 122, "SciTech");

    System.out.println(employee1.getLastName());
    System.out.println(employee1.hashCode());

    System.out.println(employee1);

    Person person1 = employee1;

    System.out.println(person1.getLastName());
//    System.out.println(person1.getId());

    System.out.println(person1.getClass());

    System.out.println(person1);

    if (person1.getClass().equals(Employee.class)) {
      Employee employee2 = (Employee) person1;
      
    }

  }

}