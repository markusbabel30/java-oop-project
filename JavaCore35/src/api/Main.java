package api;

import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {
		String s = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Трактат «Сунь-цзы», авторство которого традиционно приписывается полководцу Сунь У, с древности почитался в Китае как лучший свод, summum bonum военной мудрости.";
		String result = s.codePoints().filter(c -> (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
		System.out.println(result);
	}
}
