package api;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Main {
	public static void main(String[] args) {
		List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);

		// Using the reduce method with an identity element
		Optional<Integer> max = integers.stream().reduce(Integer::max);
		System.out.println("Max using reduce with identity: " + max.get());

		// Using the reduce method with an initial value and a BinaryOperator
		Integer max2 = integers.stream().reduce(Integer.MIN_VALUE, Integer::max);
		System.out.println("Max using reduce with initial value: " + max2);
	}
}
