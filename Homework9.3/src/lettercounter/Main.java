package lettercounter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        // Read the text from the file
        BufferedReader reader = new BufferedReader(new FileReader("text.txt"));
        StringBuilder sb = new StringBuilder();
        String line = reader.readLine();
        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = reader.readLine();
        }
        String text = sb.toString();

        // Calculate the frequency of each letter
        Map<Character, Integer> frequency = new HashMap<>();
        for (char c : text.toCharArray()) {
            if (Character.isLetter(c)) {
                c = Character.toLowerCase(c);
                if (!frequency.containsKey(c)) {
                    frequency.put(c, 1);
                } else {
                    frequency.put(c, frequency.get(c) + 1);
                }
            }
        }

        // Sort the frequency map in descending order
        List<Map.Entry<Character, Integer>> list = new LinkedList<>(frequency.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Character, Integer>>() {
            @Override
            public int compare(Map.Entry<Character, Integer> o1, Map.Entry<Character, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        // Print the result
        Map<Character, Integer> sortedFrequency = new LinkedHashMap<>();
        for (Map.Entry<Character, Integer> entry : list) {
            sortedFrequency.put(entry.getKey(), entry.getValue());
        }
        for (Map.Entry<Character, Integer> entry : sortedFrequency.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
