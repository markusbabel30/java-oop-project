package sample;

import java.lang.reflect.Field;

public class Main {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    Cat cat1 = new Cat("Luska", 8);
    Cat cat2 = new Cat("Luska", 8);

    System.out.println(cat1);

    System.out.println(cat1 == cat2);
    System.out.println(cat1.equals(cat2));

    System.out.println(cat1.hashCode());
    System.out.println(cat2.hashCode());
    System.out.println();

    Cat cat3 = null;
    try {
      cat3 = cat1.clone();
    } catch (CloneNotSupportedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println(cat3 != cat1);
    System.out.println(cat1.getClass());
    System.out.println(cat3.getClass());

    System.out.println(cat1.equals(cat3));
    System.out.println(cat3);
    System.out.println();

    Class catClass = Cat.class;
    Field[] catFields = catClass.getDeclaredFields();
    for (int i = 0; i < catFields.length; i++) {
      System.out.println(catFields[i]);
    }
    System.out.println();
    try {
      Field catAge = catClass.getDeclaredField("age");
      catAge.setAccessible(true);
      catAge.setInt(cat1, 18);
    } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
      e.printStackTrace();
    }
    System.out.println(cat1);
  }

}