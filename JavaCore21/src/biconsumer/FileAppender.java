package biconsumer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.BiConsumer;

public class FileAppender implements BiConsumer<String, File> {

	@Override
	public void accept(String s, File file) {
		try (FileWriter fw = new FileWriter(file, true);) {
			fw.write(s + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		BiConsumer<String, File> appender = new FileAppender();
		appender.accept("Hello World!", new File("output.txt"));
	}
}
