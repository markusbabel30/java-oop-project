package list;

import java.util.ArrayList;
import java.util.List;

public class ListManipulation {
  public static void main(String[] args) {
    List<Integer> list = new ArrayList<>();

    // Add 10 integers to the list
    for (int i = 0; i < 10; i++) {
      list.add(i);
    }

    // Remove the first two elements and the last element
    list.remove(0);
    list.remove(0);
    list.remove(list.size() - 1);

    // Print the resulting list
    System.out.println(list);
  }
}