package api;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	private static class Student {
		private String name;
		private int age;
		private String groupName;
		private String gender;
		private int id;

		public Student(String name, int age, String groupName, String gender, int id) {
			this.name = name;
			this.age = age;
			this.groupName = groupName;
			this.gender = gender;
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public int getAge() {
			return age;
		}

		public String getGroupName() {
			return groupName;
		}

		public String getGender() {
			return gender;
		}

		public int getId() {
			return id;
		}

		@Override
		public String toString() {
			return "Student{" + "name='" + name + '\'' + ", age=" + age + ", groupName='" + groupName + '\''
					+ ", gender='" + gender + '\'' + ", id=" + id + '}';
		}
	}

	public static void main(String[] args) {
		Student[] students = new Student[] { new Student("Eminem", 41, "Rap Godz", "Male", 1),
				new Student("Lil Wayne", 38, "Young Money", "Male", 2), new Student("Drake", 34, "OVO", "Male", 3),
				new Student("Kendrick Lamar", 36, "TDE", "Male", 4),
				new Student("Cardi B", 28, "Invasion of Privacy", "Female", 5),
				new Student("Nicki Minaj", 40, "Queen", "Female", 6),
				new Student("J. Cole", 37, "Dreamville", "Male", 7),
				new Student("Travis Scott", 29, "Astroworld", "Male", 8),
				new Student("Post Malone", 25, "Hollywood's Bleeding", "Male", 9),
				new Student("Megan Thee Stallion", 27, "Good News", "Female", 10) };

		System.out.println("Students before sorting: ");
		for (Student student : students) {
			System.out.println(student);
		}

		List<Student> olderThan20 = Arrays.stream(students).filter(s -> s.getAge() > 20)
				.sorted((s1, s2) -> s1.getName().compareTo(s2.getName())).collect(Collectors.toList());

		System.out.println("Students over twenty after sorting: ");
		for (Student student : olderThan20) {
			System.out.println(student);
		}
	}
}