package predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

class StringList {
	private List<String> list;

	public StringList() {
		list = new ArrayList<>();
	}

	public void add(String s) {
		list.add(s);
	}

	public void removeStartingWith(char c) {
		Predicate<String> startsWith = s -> s.startsWith(String.valueOf(c));
		list.removeIf(startsWith);
	}

	public List<String> getList() {
		return list;
	}
}

class Example {
	public static void main(String[] args) {
		StringList list = new StringList();
		list.add("First");
		list.add("Second");
		list.add("Third");
		list.add("Fourth");
		list.add("Fifth");

		list.removeStartingWith('F');

		System.out.println(list.getList());
	}
}
