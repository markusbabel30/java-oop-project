package comparable;

import java.util.Arrays;

class Cat implements Comparable<Cat> {
	private String name;
	private int age;

	public Cat(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Cat)) {
			return false;
		}
		Cat cat = (Cat) obj;
		return cat.name.length() == name.length() && cat.age == age;
	}

	@Override
	public int compareTo(Cat other) {
		int nameLengthComparison = Integer.compare(this.name.length(), other.name.length());
		if (nameLengthComparison != 0) {
			return nameLengthComparison;
		}
		return Integer.compare(this.age, other.age);
	}
}

public class Main {
	public static void main(String[] args) {
		Cat[] cats = new Cat[] { new Cat("Whiskers", 3), new Cat("Whispers", 3), new Cat("Luna", 2), new Cat("Max", 7),
				new Cat("Oliver", 1), new Cat("Charlie", 4), new Cat("Simba", 6), new Cat("Mittens", 8),
				new Cat("Socks", 9), new Cat("Fluffy", 10) };
		Arrays.sort(cats);
		Cat longestNameCat = cats[cats.length - 1];
		System.out.println("Cat with the longest name is " + longestNameCat.getName() + " and it's age is "
				+ longestNameCat.getAge());
	}
}