package binaryoperator;

import java.util.List;
import java.util.function.BinaryOperator;

public class MinList<T extends Comparable<T>> implements BinaryOperator<List<T>> {

    @Override
    public List<T> apply(List<T> list1, List<T> list2) {
        T min1 = list1.stream().min(T::compareTo).get();
        T min2 = list2.stream().min(T::compareTo).get();
        if (min1.compareTo(min2) <= 0) {
            return list1;
        } else {
            return list2;
        }
    }
}
