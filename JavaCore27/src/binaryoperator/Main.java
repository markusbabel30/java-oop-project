package binaryoperator;

import java.util.Arrays;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		MinList<Integer> minList = new MinList<>();
		List<Integer> list1 = Arrays.asList(5, 0, 3, 4);
		List<Integer> list2 = Arrays.asList(10, -2, 5);
		List<Integer> min = minList.apply(list1, list2);
		System.out.println(min);
	}
}
