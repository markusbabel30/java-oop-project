package groupOfStudents;

import java.io.*;
import java.util.List;

public class GroupFileStorage {
	public void saveGroupToCSV(Group group) throws IOException {
		File file = new File(group.getGroupName() + ".csv");
		List<Student> students = group.getStudents();
		CSVStringConverter csv = new CSVStringConverter();

		try (FileWriter fw = new FileWriter(file)) {
			for (Student student : students) {
				if (student != null) {
					fw.append(csv.toStringRepresentation(student)).append(System.lineSeparator());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Group loadGroupFromCSV(File file) throws IOException {
		Group group = new Group(file.getName().substring(0, file.getName().lastIndexOf(".")));
		List<Student> students = group.getStudents();
		CSVStringConverter csv = new CSVStringConverter();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String studentCSV;
			while ((studentCSV = reader.readLine()) != null) {
				students.add(csv.fromStringRepresentation(studentCSV));
			}
		} catch (IOException e) {
			return null;
		}
		group.setStudents(students);
		return group;
	}

	public File findFileByGroupName(String groupName, File workFolder) {
		File[] files = workFolder.listFiles();

		if (files != null) {
			for (File file : files) {
				if (file.isFile() && file.getName().equals(groupName + ".csv")) {
					return file;
				}
			}
		}

		return null;
	}
}
