package groupOfStudents;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		String groupName = "MyGroup";
		List<Student> students = new ArrayList<>();
		Group group = new Group(groupName, students);

		addStudentToGroup(group, new Student("Bodak", "Yellow", Gender.MALE, 1, groupName));
		addStudentToGroup(group, new Student("Snoop", "Dogg", Gender.MALE, 2, groupName));
		addStudentToGroup(group, new Student("Ice", "Cube", Gender.MALE, 3, groupName));
		addStudentToGroup(group, new Student("Missy", "Elliott", Gender.FEMALE, 4, groupName));
		addStudentToGroup(group, new Student("Snoop", "Dogg", Gender.MALE, 5, groupName));
		addStudentToGroup(group, new Student("Remy", "Ma", Gender.FEMALE, 6, groupName));
		addStudentToGroup(group, new Student("Gwen", "Stefani", Gender.FEMALE, 7, groupName));
		addStudentToGroup(group, new Student("Iggy", "Azalea", Gender.FEMALE, 8, groupName));
		addStudentToGroup(group, new Student("Kanye", "West", Gender.MALE, 9, groupName));
		addStudentToGroup(group, new Student("Kendrick", "Lamar", Gender.MALE, 10, groupName));

		printGroupInfo(group);

		GroupFileStorage gfs = new GroupFileStorage();

		try {
			gfs.saveGroupToCSV(group);
		} catch (IOException e) {
			e.printStackTrace();
		}

		File file = new File("./MyGroup.csv");
		Group group2;
		try {
			group2 = gfs.loadGroupFromCSV(file);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		printGroupInfo(group2);

		File workFolder = new File(".");
		System.out.println(gfs.findFileByGroupName("MyGroup", workFolder));
		Group.printEquivalentStudents(group);

		removeStudentFromGroup(group, 2);
		removeStudentFromGroup(group, 11);
		printGroupInfo(group);
	}

	private static void printGroupInfo(Group group) {
		System.out.println("Group name: " + group.getGroupName());
		System.out.println("Number of students: " + group.getStudents().size());
		System.out.println("Students: ");
		for (Student student : group.getStudents()) {
			System.out
					.println("  " + student.getName() + " " + student.getLastName() + " (ID: " + student.getId() + ")");
		}
	}

	private static void addStudentToGroup(Group group, Student student) {
		try {
			group.addStudent(student);
		} catch (GroupOverflowException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	private static void removeStudentFromGroup(Group group, int id) {
		if (group.removeStudentByID(id)) {
			System.out.println("Successfully removed student with ID " + id + " from group " + group.getGroupName());
		} else {
			System.out.println("Student with ID " + id + " not found in group " + group.getGroupName());
		}
	}
}