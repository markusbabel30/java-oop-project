package unaryperator;

import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;

public class Main {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		System.out.println("Original List: " + list);
		UnaryOperator<Integer> replaceOddWithZero = num -> num % 2 != 0 ? 0 : num;
		list.replaceAll(replaceOddWithZero);
		System.out.println("Modified List: " + list);
	}
}
