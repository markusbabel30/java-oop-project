package shipsanddocks;

import java.util.concurrent.Semaphore;

public class ShipUnloading {
	// Number of boxes per ship
	public static final int NUM_BOXES = 10;

	// Semaphore to control access to the docks
	public static Semaphore docks = new Semaphore(2);

	public static void main(String[] args) throws InterruptedException {
		// Create three ships
		Ship ship1 = new Ship("Ship 1");
		Ship ship2 = new Ship("Ship 2");
		Ship ship3 = new Ship("Ship 3");

		// Start unloading the cargo from each ship in a separate thread
		new Thread(ship1).start();
		new Thread(ship2).start();
		new Thread(ship3).start();
	}
}

class Ship implements Runnable {
	private String name;

	public Ship(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		try {
			// Acquire a dock
			ShipUnloading.docks.acquire();

			System.out.println(name + " is unloading cargo using a dock.");

			// Unload the cargo
			for (int i = 0; i < ShipUnloading.NUM_BOXES; i++) {
				Thread.sleep(500); // unloading a box takes 0.5 sec
				System.out.println(name + ": Unloaded box " + (i + 1));
			}

			// Release the dock
			ShipUnloading.docks.release();

			System.out.println(name + " has finished unloading all its cargo.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
