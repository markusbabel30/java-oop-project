package comparison;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class FileComparisonService {
	public static void main(String[] args) {
		// Use a scanner to read file addresses from the user
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the first file path: ");
		String file1Path = scanner.nextLine();
		System.out.print("Enter the second file path: ");
		String file2Path = scanner.nextLine();

		// Compare the files using the compareFiles method
		boolean areIdentical = compareFiles(file1Path, file2Path);
		if (areIdentical) {
			System.out.println("The files are identical.");
		} else {
			System.out.println("The files are different.");
		}
	}

	public static boolean compareFiles(String file1Path, String file2Path) {
		// Create File objects for the two files
		File file1 = new File(file1Path);
		File file2 = new File(file2Path);

		// Check if the files have the same length
		if (file1.length() != file2.length()) {
			return false;
		}

		// Open input streams for the two files
		try (FileInputStream fis1 = new FileInputStream(file1); FileInputStream fis2 = new FileInputStream(file2)) {
			// Compare the files byte by byte
			int b1, b2;
			do {
				b1 = fis1.read();
				b2 = fis2.read();
				if (b1 != b2) {
					return false;
				}
			} while (b1 != -1);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
