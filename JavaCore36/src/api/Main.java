package api;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Cat {
	private String name;
	private String color;
	private int weight;

	public Cat(String name, String color, int weight) {
		setName(name);
		setColor(color);
		setWeight(weight);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid cat name");
		}
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		if (color == null || color.trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid cat color");
		}
		this.color = color;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		if (weight <= 0) {
			throw new IllegalArgumentException("Invalid cat weight");
		}
		this.weight = weight;
	}

	public String toString() {
		return "Name: " + name + " Color: " + color + " Weight: " + weight;
	}
}

public class Main {
	public static void main(String[] args) {
		List<Cat> cats = new ArrayList<>();
		try {
			cats.add(new Cat("Whiskers", "black", 1));
			cats.add(new Cat("Socks", "white", 2));
			cats.add(new Cat("Fluffy", "gray", 5));
			cats.add(new Cat("Mittens", "orange", 3));
			cats.add(new Cat("Smokey", "black", 6));
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			return;
		}

		System.out.println("Original List: " + cats);

		List<Cat> filteredCats = cats.stream().filter(cat -> cat.getWeight() >= 3)
				.sorted((cat1, cat2) -> cat1.getName().compareTo(cat2.getName())).collect(Collectors.toList());

		System.out.println("Filtered and Sorted List: " + filteredCats);
	}
}
