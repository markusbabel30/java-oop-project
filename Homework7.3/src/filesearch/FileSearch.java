package filesearch;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class FileSearch {
    public static void main(String[] args) throws InterruptedException {
        // Read the file name and search directory from the standard input
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the file name: ");
        String fileName = scanner.nextLine();
        System.out.print("Enter the search directory: ");
        String searchDirectory = scanner.nextLine();
        scanner.close();

        // Create a list to store the search results
        ArrayList<String> results = new ArrayList<>();

        // Start the search operation in a separate thread
        SearchThread searchThread = new SearchThread(fileName, searchDirectory, results);
        new Thread(searchThread).start();

        // Wait for the search to finish
        while (searchThread.isRunning()) {
            Thread.sleep(500);
        }

        // Display the search results
        if (results.isEmpty()) {
            System.out.println("No files with the name " + fileName + " were found.");
        } else {
            System.out.println("Files with the name " + fileName + " were found in the following locations:");
            for (String result : results) {
                System.out.println(result);
            }
        }
    }
}

class SearchThread implements Runnable {
    private String fileName;
    private String searchDirectory;
    private ArrayList<String> results;
    private boolean running;

    public SearchThread(String fileName, String searchDirectory, ArrayList<String> results) {
        this.fileName = fileName;
        this.searchDirectory = searchDirectory;
        this.results = results;
        this.running = true;
    }

    public boolean isRunning() {
        return running;
    }

    @Override
    public void run() {
        search(new File(searchDirectory));
        running = false;
    }

    private void search(File file) {
        if (file.isDirectory()) {
            // Search all the files in the directory
            File[] files = file.listFiles();
            if (files != null) {
                for (File f : files) {
                    search(f);
                }
            }
        } else if (file.getName().equals(fileName)) {
            // Add the file to the search results
            results.add(file.getAbsolutePath());
        }
    }
}
