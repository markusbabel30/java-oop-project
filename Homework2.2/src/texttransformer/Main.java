package texttransformer;

import java.io.File;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the text");
		String str = sc.nextLine();
		TextTransformer tr = new TextTransformer();
		System.out.println(tr.transform(str));
		tr = new InverseTransformer();
		System.out.println(tr.transform(str));
		tr = new UpDownTransformer();
		System.out.println(tr.transform(str));

		String file = "text.txt";
		TextSaver textSaver = new TextSaver(new UpDownTransformer(), new File(file));
		textSaver.saveTextToFile(str);
		sc.close();
	}
}